#!/usr/bin/env bash

# set default values
DEFAULT_OUTDIR='.'
DEFAULT_CREATE_SUBDIR='false'
DEFAULT_TEMPLATE='{time:%Y-%m-%dT%H-%M-%S} - {author} - {title} - {id}.ts'
DEFAULT_QUALITY='best'
DEFAULT_TIMEOUT=60
DEFAULT_STREAMLINK_EXECUTABLE='streamlink'

# vars
OUTDIR=${SR_OUTDIR:-${DEFAULT_OUTDIR}}
CREATE_SUBDIR=${SR_CREATE_SUBDIR:-${DEFAULT_CREATE_SUBDIR}}
TEMPLATE=${SR_TEMPLATE:-${DEFAULT_TEMPLATE}}
QUALITY=${SR_QUALITY:-${DEFAULT_QUALITY}}
TIMEOUT=${SR_TIMEOUT:-${DEFAULT_TIMEOUT}}
URL=${SR_URL}
STREAMLINK_EXECUTABLE=${SR_STREAMLINK_EXECUTABLE:-${DEFAULT_STREAMLINK_EXECUTABLE}}
declare -a STREAMLINK_ARGS=${SR_STREAMLINK_ARGS}

# parse commandline arguments
ARGS=$(getopt --options 'o:q:t:f:e:dh' --longoptions 'outdir:,quality:,timeout:,template:,streamlink-executable:,--create-subdir,help' -- "${@}") || exit
eval set -- "${ARGS}"
unset ARGS

function print_help {
	local EXE_NAME
	EXE_NAME="$(basename "$0")"
	echo "${EXE_NAME}"
	echo ""
	echo "USAGE:"
	echo "${EXE_NAME} [OPTIONS] TWITCH_USER|STREAM_URL"
	echo ""
	echo "Some arguments and options can also by set by env vars, see names in parethesis."
	echo ""
	echo "ARGS:"
	echo "  <TWITCH_USER|STREAM_URL> (SR_URL)"
	echo "    Either a stream URL that is supported by streamlink or the username of a Twitch.tv streamer."
	echo ""
	echo "OPTIONS:"
	echo "  -o, --outdir <DIR>"
	echo "    Directory to save streams to. Will be created if it does not exist."
	echo ""
	echo "  -d, --create-subdir"
	echo "    Create a subdir in outdir named after the captured channel's name."
	echo ""
	echo "  -f, --template <TEMPLATE>"
	echo "    Filename template, defaults to »${DEFAULT_TEMPLATE}«."
	echo ""
	echo "  -q, --quality <SPEC>"
	echo "    Quality specification as understood by streamlink. Defaults to »${DEFAULT_QUALITY}«."
	echo ""
	echo "  -t, --timeout <SECONDS>"
	echo "    Time to wait between stream start checking. Defaults to »${DEFAULT_TIMEOUT}«."
	echo ""
	echo "  -e, --streamlink-executable <PATH>"
	echo "    Path to the streamlink binary. Defaults to »${DEFAULT_STREAMLINK_EXECUTABLE}«. Will be searched in \$PATH."
	echo ""
	echo "  -h, --help"
	echo "    Print this text. Exit."
	echo ""
	# TBD: STREAMLINK_ARGS
}

while true; do
	case "$1" in
		'-o'|'--outdir')
			OUTDIR="$2"
			shift 2
			continue
		;;
		'-d'|'--create-subdir')
			CREATE_SUBDIR='true'
			shift 1
			continue
		;;
		'-q'|'--quality')
			QUALITY="$2"
			shift 2
			continue
		;;
		'-t'|'--timeout')
			TIMEOUT="$2"
			shift 2
			continue
		;;
		'-f'|'--template')
			TEMPLATE="$2"
			shift 2
			continue
		;;
		'-h'|'--help')
			print_help
			exit 0
		;;
		'--')
			shift
			break
		;;
		*)
			echo >&2 'Error parsing commandline arguments!'
			exit 10
		;;
	esac
done

# read stream url, fallback to twitch on non-url
if [[ "$1" == *"://"* ]]; then
	URL="$1"
else
	URL="twitch.tv/$1"
fi

# exit if URL is empty
[ -z "${URL}" ] && { echo >&2 'Error: No URL/username given'; exit 20; }

# check that TIMEOUT is a valid number
[[ $TIMEOUT =~ ^[0-9]+$ ]] || { echo >&2 "Error: timeout value »${TIMEOUT}« is not a positive integer"; exit 50; }

# find streamlink executable
command -v "${STREAMLINK_EXECUTABLE}" >/dev/null 2>&1 || { echo >&2 "Error: unable to find streamlink executable"; exit 30; }

# canonicalize OUTDIR (strip / from end)
OUTDIR=$(realpath "${OUTDIR}")

# append subdir
# workaround for https://github.com/streamlink/streamlink/issues/4411
if [ "${CREATE_SUBDIR}" == 'true' ];  then
	USERNAME="$(echo "${URL%%/}" | rev | cut -d/ -f 1 | rev)"
	OUTDIR="${OUTDIR}/${USERNAME}"
fi

# make sure OUTDIR exists
mkdir -p "${OUTDIR}"
{ [ -d "${OUTDIR}"  ] && [ -w "${OUTDIR}" ]; } || { echo >&2 "Error: »${OUTDIR}« is not a writable directory"; exit 40; }

# loop forever and wait for stream to sstart
while true; do
	echo -n "$(date "+%Y-%m-%d %H:%M:%S") Checking if ${URL} is streaming… "
	if streamlink --quiet "${URL}"> /dev/null 2>&1; then
		echo "they are streaming 🤩."
		streamlink "${STREAMLINK_ARGS[@]}" --output "${OUTPUT}/${TEMPLATE}" --force "${URL}" "${QUALITY}"
	else
		echo "they are not streaming 🫠."

	fi
	echo "😴 Sleeping for ${TIMEOUT} seconds."
	sleep "${TIMEOUT}"
done
